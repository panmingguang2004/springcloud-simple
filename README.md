### 技术架构

- springcloud精简项目
- 包括eureka注册中心, 所有的服务 都会注册进来, 包括 feign ribbon
- Service提供服务端,并直接暴露http端口调用
- ribbon[客户端负载均衡器] -feign[ribbon+restTemp] - netflix[熔断机制组件]
- zuul -功能是路由转发和过滤器
- config-  配置中心,未集成
		
## 调用链

	zuul --> ribbon+feign+netflix--> service 
	 |            |                    |
	 |____________|____________________|                   
                  |
                  eureka 

### 对比nginx

#### 1) 负载均衡  

- springcloud: ribbon+feign+netflix
- nginx: upstream 反向代理负载均衡
	
	
#### 2) 路由转发和过滤

- springcloud: zuul 新版本的 spring-gateway  
- nginx: 自带路由配置服务器转发 和过滤 location 正则过滤, 表达式过滤等
	
#### 3) 注册中心

- springcloud: eureka 可以监控服务的状态
- nginx: 自带无
	
#### 4) 限流

- springcloud: 最新spring-gateway 带功能
- nginx: 默认安装带 桶算法限流 
	 
#### 5) HA

- springcloud: 主要是注册中心 HA, 网关HA, 网关HA 一般用Nginx配置服务器代理
- nginx: 自带无, 可使用keepalive 配置HA
	
#### 6) 静态代理服务器

- springlcoud: 使用nginx 或其他 apach 等其他静态服务器
- nginx: 自带
	
