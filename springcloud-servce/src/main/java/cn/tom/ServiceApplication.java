package cn.tom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

//@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"cn.tom.control", "cn.tom.service"})
@EnableEurekaClient
//@EnableDiscoveryClient //consule zookeeper etcd
public class ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run( ServiceApplication.class, args );
    }
   
}



