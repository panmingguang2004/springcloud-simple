package cn.tom.control;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.tom.service.HelloService;

@RestController
public class HelloController {
	
	@Resource
	HelloService helloService;
	
	@RequestMapping("/hello")
	public String hell(@RequestParam String name){
		return helloService.hello(name);
	}

}
