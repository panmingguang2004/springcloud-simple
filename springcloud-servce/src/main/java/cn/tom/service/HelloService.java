package cn.tom.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

	@Value("${server.port}")
    String port;
	
	public String hello(String name){
		
		return "hello -->" + name + ", port: "+ port;
	}
}
